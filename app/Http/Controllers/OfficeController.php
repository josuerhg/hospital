<?php

namespace App\Http\Controllers;

use App\Crud;
use App\Office;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Notification;

class OfficeController extends Controller
{
    //Lista de consultorios
  public function index()
  {

    if (Auth::user()->Patient()) {

      $offices = Office::active();


      return view('hospital.office.indexOffice', compact('offices'));
    }
    return view('admin');
  }

    //Agregar consultorio
  public function create()
  {
    if (Auth::user()->Admin()) {


      return view('hospital.office.createOffice');
    }
    return view('admin');
  }

    //Almacenar consultorio
  public function store(Request $request)
  {
    if (Auth::user()->Admin()) {



      $data = request()->validate([
        'name' => 'required|string|max:255',
        'email' => 'required|string|email|max:255|unique:users',
        'telephone' => 'required|string|max:20',
        'sex' => 'required|string|max:1',
        'image' => 'required|file',
        'password' => 'required|string|min:6|confirmed',
        'address' => 'required|string|max:255',
        'birthdate' => 'required|date',
        'postalCode' => 'required|integer|max:999999',
        'city' => 'required|string|max:255',
        'country' => 'required|string|max:255',

            //
        'map' => 'required|string',
        'name_office' => 'required|string',

      ]);

      $ruta_imagen =  $data['image']->store('patients', 'public');



            //Crear consultorio
      $office = new Office;
      $office->name = $data['name_office'];
      $office->address = $data['address'];
      $office->postalCode = $data['postalCode'];
      $office->city = $data['city'];
      $office->country = $data['country'];
      $office->map = $data['map'];

      $office->save();

      Crud::newUser($data, 'office', $office->id, $ruta_imagen);


    Notification::toAdmin( array(
            'subject'=>"Se ha creado un nuevo doctor",
            'text'=>[
                
                'Para ver sus detalles ingresa al link que hemos enviado',
                
                

            ],
            'url'=> $office->profileUrl,
            'btnText'=>'Ver consultorio'
        ));






      return redirect('/office')->with('success', '¡El consultorio ha sido agregado con éxito!');
    }

    return view('admin');
  }


    //Mostrar información
  public function show($id)
  {
    $office = Office::find($id);

    if (Auth::user()->Patient()) {

      return view('hospital.office.showOffice', compact('office'))
      ->with('doctors', $office->doctors);
    }
    return view('admin');
  }

    //Actualizar
  public function edit($id)
  {
    if (Auth::user()->Office()) {




      if (Auth::user()->isAdmin() || Auth::user()->profile()->id == $id) {
        $office = Office::find($id);
        return view('hospital.office.editOffice', compact('office'));
      }
    }
    return view('admin');
  }

    //Método update
  public function update(Request $request, Office $office)
  {


   $data = request()->validate([
    'name' => 'required|string|max:255',
    'birthdate' => 'required|date',
    'telephone' => 'required|string|max:20',
    'sex' => 'required|string|max:1',
    'email' => 'required|string|email|max:255',

    'postalCode' => 'required|integer|max:999999',
    'city' => 'required|string|max:255',
    'country' => 'required|string|max:255',
    'address' => 'required|string|max:255',

    'name_office' => 'required|string|max:255',
    'map' => 'required|string|max:255',


  ]);








   $office->address = $data['address'];
   $office->postalCode = $data['postalCode'];
   $office->city = $data['city'];
   $office->country = $data['country'];
   $office->map = $data['map'];
   $office->name = $data['name_office'];





   $office->save();




   $user = $office->user();

   $user->name = $data['name'];
   $user->telephone = $data['telephone'];
   $user->sex = strtolower($data['sex']);
   $user->birthdate = $data['birthdate'];

   $user->email = $data['email'];

   $user->save();





   return redirect('/office/' . $office->id)->with('success', '¡El consultorio ha sido actualizado con éxito!');
 }

      //Eliminar doctor
   public function destroy(Office $office)
   {
 
    if (Auth::user()->Admin()) {

      $office->user()->deactivate();
      return redirect('/doctor')->with('success', '¡El consultorio ha sido eliminado con éxito!');
    }
    return view('admin');
  }
}
