<?php

namespace App\Http\Livewire\Bi;

use Livewire\Component;

class Regression extends Component
{
    public function render()
    {
        return view('livewire.bi.regression');
    }
}
