<?php

namespace App\Http\Livewire\Bi\Doctor;

use Livewire\Component;

class Best extends Component
{
    public function render()
    {
        return view('livewire.bi.doctor.best');
    }
}
