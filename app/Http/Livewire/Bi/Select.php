<?php

namespace App\Http\Livewire\Bi;

use Livewire\Component;

class Select extends Component
{
    public function render()
    {
        return view('livewire.bi.select');
    }
}
