<div class="card caja-contador ">

	<div class="caja-contador-icono">

		<i class="fal {{$icon}}"></i>
	</div>
	<div class="card-body">


		<h3>{{count($model)}}</h3>
		<p>{{$title}}</p>
	</div>
</div>